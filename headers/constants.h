/*! \file constants.h
 * This file contains various constants needed for the project
 */

#include <armadillo>

/** \details
 * Here are defined constants usefull for the project. We defined constants for three differents modes.
 * The first one is an easy mode with all constants set at 1.
 * The second and the third are for emulate a proton or an electron.
 */

namespace constants {

    /* 
    * Constants that will be used for the easy mode.
    */
    extern double m_easy; /*!< This is m for the easy mode */
    extern double omega_easy; /*!< This is omega for the easy mode */
    extern double h_bar_easy; /*!< This is h_bar for the easy mode */

    /*
     * Definition of m, omega and h_bar for proton mode
     */
    extern double m_proton; /*!< This is m for the proton mode */
    extern double omega_proton; /*!< This is omega for the proton mode */
    extern double h_bar_hard;  /*!< This is h_bar for the proton and electron mode */

    /*
     * Definition of m and omega for electron mode
     */
    extern double m_electron; /*!< This is m for the electron mode */
    extern double omega_electron; /*!< This is omega for the electron mode */

    /*
    * Definition of 2 armadillo matrix, one full of 70 positions et the other with 70 weights
    * They are associated with the Gauss-Hermite quadrature
    * It will be used for the calculus of various integral 
    */

    /**
     * The matrix of 70 positions used for the Gauss-Hermite quadrature.
     * The pos(i) value is the position associated with the weight(i) value for this quadrature*/
    extern arma::mat pos;

    /** The matrix of 70 weights used for the Gauss Hermite quadrature.
     * The weight(i) value is the weight associated with the pos(i) value for this quadrature*/
    extern arma::mat weight;
}