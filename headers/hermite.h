/*! \file hermite.h
 * Here is defined the Hermite class, used for the Hermite's polynomials
 */
#include <armadillo>

/** \details
 * Here is defined the Hermite class, used for the Hermite's polynomials
 */
class Hermite
{
    /** The attribute that will contains various Hermite's polynomials */
    private:
        arma::mat Hn;
    /** The functions used in order to set up and get Hermite's Polynomials */
    public:
        Hermite(int nmax, arma::rowvec vectcoord);
	    arma::mat getHermite();
};
