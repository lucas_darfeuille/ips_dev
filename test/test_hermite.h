#include <cxxtest/TestSuite.h>
#include <armadillo>
#include <iostream>
#include "../headers/hermite.h"
#include "../headers/constants.h"

class TestHermite : public CxxTest::TestSuite
{
  public:
  void testOrthonormality(void)
  {

    double epsilon = pow(10,-14);

    //n is the number of solutions
    int n = 70;

    //Initializing Hermite's polynome with the position from constants::pos 
    arma::mat hermite_n = Hermite(n,constants::pos).getHermite();

    //Defining ConstantMatrix used to calculate the solutions
    arma::mat constantMatrix = arma::mat(n,n,arma::fill::zeros);
    
    double cst_fact = 1; 
    constantMatrix.at(0,0)= cst_fact;
    for(int i = 1; i < n ; i++)
    {
      cst_fact = cst_fact *  1/(sqrt( 2 * i));
      constantMatrix(i,i) = cst_fact;
    }

    hermite_n = constantMatrix * hermite_n;
    double integralApproximation ;
    for (int i = 0; i < n; i++) 
    {
      for (int j = 0; j < n; j++) 
      {
        integralApproximation = (arma::accu((hermite_n.row(i) % hermite_n.row(j)) % constants::weight)) * (1/(sqrt(arma::datum::pi)));
        if(i==j) 
        {
          TS_ASSERT_DELTA(integralApproximation,1,epsilon);
        }
        else 
        {
          TS_ASSERT_DELTA(integralApproximation,0,epsilon);
        }
      }
    }

  

  }
};
