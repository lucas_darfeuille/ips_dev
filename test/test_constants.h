#include <cxxtest/TestSuite.h>
#include <armadillo>
#include <iostream>
#include "../headers/constants.h"

class TestConstantes : public CxxTest::TestSuite
{
  public:
  void testSumWeightHermite(void)
  {
    double sumWeight = arma::accu(constants::weight);

    double epsilon = pow(10,-14);
    TS_ASSERT_DELTA(sumWeight,sqrt(arma::datum::pi),epsilon);
  }
};