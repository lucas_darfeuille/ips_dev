/*! \file hermite.cpp
 * The implementation of hermite.h, with the use of armadillo
 */

#include "../headers/hermite.h"
/**
 * Definition of the constructor of Hermite class
 * 
 * This constructor create a matrix.
 * 
 * We use a recurrence relation to calculate all the values, row by row and the 
 * last row is what we calculcate the matrix for. 
 * 
 * @param nmax the number of solutions
 * @param vectcoord an array of coordinates for which we will do the operation
 */
Hermite::Hermite(int nmax, arma::rowvec vectcoord)
{
    arma::mat Ha(nmax, vectcoord.n_cols);
   
    Ha.row(0).ones();
    if(nmax>1)
    {
        Ha.row(1) = 2 * vectcoord;
        for(int n = 2 ; n < nmax ; n++)
        {
            Ha.row(n) = 2 * vectcoord % Ha.row(n-1) - 2 * (n-1) * Ha.row(n-2);
        }
        
    }

    this->Hn = Ha;
}

/**
 * This functions is used in order to get Hermite's Polynomials
 */
arma::mat Hermite::getHermite()
{
    return Hn;
}

