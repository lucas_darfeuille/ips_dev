/*! \file main.cpp
 * The main function executed by a user. It implements the solutions of the Schrodinger equation in 1 dimension
 * There is no return value. The solutions asked can be found in the file "all_solution.csv"
 */

#include <iostream>
#include <string> 
#include <armadillo>
#include "../headers/constants.h"  
#include "../headers/hermite.h"
using namespace std;

/** \details
 * 
 * This function does not plot the solutions, it only create the .csv file with all the solutions 
 * of the Schrodinguer equation between the first one and the n-th one ; assuming n is the number of solution
 * the user gave.
 */

int main(int argc, char const *argv[])
{
    /*
     * System will be used for asking users the number of solutions they want, nsolution will store that answer
     */
    int system, nsolution;
    double m, omega, h_bar, cst_calc, cst_expo, cst_herm, cst_fact, h;
    arma::rowvec veccoord(5001); // Values for z
    arma::rowvec hermite_pos(5001); // Values for Hermite

    cout << "What system do you want :" << endl;
    cout << " 1- easy mode" << endl;
    cout << " 2- proton" << endl;
    cout << " 3- electron" << endl;
    cin >> system;


    if (cin.eof() || cin.bad()) 
    {
        cerr << "(user canceled or unreconverable error)\n";
        return 1;
    }
    while (cin.fail() || system < 0 || system >3) {

        cin.clear();          
        cerr << "error: invalid input. Please enter an integer between 1 and 3\n";
        cin.ignore(numeric_limits<streamsize>::max(), '\n');

        cout << "What system do you want :" << endl;
        cout << " 1- easy mode" << endl;
        cout << " 2- proton" << endl;
        cout << " 3- electron" << endl;
        cin >> system;
    }


    
    /*
     * Here are set the values for m, omega and h_bar, following the user instructions with :
     * m in Kg
     * omega in Hz
     * h_bar in m2.kg/s
     */
    if(system == 1)
    {
        h=0.01;
        m = constants::m_easy;
        omega = constants::omega_easy;
        h_bar = constants::h_bar_easy;
    }
    else if(system == 2)
    { 
        h = 1e-11;
        m = constants::m_proton;
        omega = constants::omega_proton;
        h_bar = constants::h_bar_hard;
    }
    else 
    {
        h = 1e-11;
        m = constants::m_electron;
        omega = constants::omega_electron;
        h_bar = constants::h_bar_hard;
    }

    /*
     * Definition of constants needed to the calculus of our solutions
     */
    cst_calc = pow((m * omega / (arma::datum::pi*h_bar)), 0.25); //Constant appearing in all psi_n
        
    cst_expo = - m * omega / (2 * h_bar);  //Constant appearing in the exponential in all psi_n

    cst_herm = pow(m * omega / h_bar, 0.5); //Constant appearing in Hermite polynomials

    cst_fact = 1.;

    /*
     * Setting the hermite_pos vector in order to call the Hermite constructor (with h the difference between two positions)
     */
    for(int i = 1 ; i < 2501 ; i++)
    { //Loop in order to caclulate all z
        veccoord(2500 + i) = veccoord(2500 + i - 1) + h;
        veccoord(2500 - i) = veccoord(2500 - i + 1) - h;
    } 

    // for(int i = 0 ; i < 24001 ; i++)
    // {
    //     hermite_pos(i) = veccoord(i) * cst_herm;
    // } 
    //TOREV
    hermite_pos = veccoord * cst_herm;

    cout << "How many solutions do you want ?" << endl;
    cin >> nsolution;
    
    
    if (cin.eof() || cin.bad()) 
    {
        cerr << "(user canceled or unreconverable error)\n";
        return 1;
    }
    while (cin.fail() || nsolution<=0) {

        cin.clear();          
        cerr << "error: invalid input. Please enter a strictly positive integer\n";
        cin.ignore(numeric_limits<streamsize>::max(), '\n');

        cout << "How many solutions do you want ?" << endl;
        cin >> nsolution;
    }


    /*
     * Solutions will be in a matrix, with a row by solution, evaluated in every z 
     */
    arma::mat all_solution(nsolution, 5001);
    Hermite H(nsolution, hermite_pos);
    
    all_solution = H.getHermite();


    arma::mat cstFactMatrix = arma::mat(nsolution,nsolution,arma::fill::zeros);
    
    cst_fact = 1; 
    cstFactMatrix.at(0,0)= cst_fact;
    for(int i = 1; i < nsolution ; i++)
    {
      cst_fact = cst_fact *  1/(sqrt( 2 * i));
      cstFactMatrix(i,i) = cst_fact;
    }

    arma::mat cstExpMatrix = arma::mat(nsolution,5001,arma::fill::zeros);

    for( int j =0; j< nsolution; j++) 
    {
        cstExpMatrix.row(j) = exp(cst_expo * (veccoord % veccoord));
        
    }


    all_solution = cst_calc * cstFactMatrix * all_solution % cstExpMatrix;

    /*
     * All solution calculated are in the "all_solution.csv" file. The user is free to plot the solutions themself
     */
    all_solution.save("all_solution.csv", arma::csv_ascii );
    return 0;




}
